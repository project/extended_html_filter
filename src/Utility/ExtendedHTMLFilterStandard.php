<?php

namespace Drupal\extended_html_filter\Utility;

use Drupal\editor\EditorXssFilter\Standard;

class ExtendedHTMLFilterStandard extends Standard{
  protected static function split($string, $html_tags, $class){
    if (strpos($string, '<') !== 0) {
      // We matched a lone ">" character.
      return '&gt;';
    }

    if (strlen($string) === 1) {
      // We matched a lone "<" character.
      return '&lt;';
    }

    if (!preg_match('%^<\s*(/\s*)?([a-zA-Z0-9\-]+)\s*([^>]*)>?|(<!--.*?-->)$%', $string, $matches)) {
      // Seriously malformed.
      return '';
    }

    $slash = trim($matches[1]);
    $elem = &$matches[2];
    $attributes = &$matches[3];
    $comment = &$matches[4];

    if ($comment) {
      $elem = '!--';
    }

    // Defer to the ::needsRemoval() method to decide if the element is to be
    // removed. This allows the list of tags to be treated as either a list of
    // allowed tags or a list of denied tags.
    if (self::needsRemoval($html_tags, $elem)) {
      return '';
    }

    if ($comment) {
      return $comment;
    }

    if ($slash !== '') {
      return "</$elem>";
    }

    // Is there a closing XHTML slash at the end of the attributes?
    $attributes = preg_replace('%(\s?)/\s*$%', '\1', $attributes, -1, $count);
    $xhtml_slash = $count ? ' /' : '';

    // Clean up attributes.
    $attr2 = implode(' ', ExtendedHTMLFilterXss::attributes($attributes));
    $attr2 = preg_replace('/[<>]/', '', $attr2);
    $attr2 = $attr2 !== '' ? ' ' . $attr2 : '';

    return "<$elem$attr2$xhtml_slash>";
  }

}

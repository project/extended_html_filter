<?php

namespace Drupal\extended_html_filter\Utility;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Xss;

/**
 * Provides helper to filter for cross-site scripting.
 *
 * @ingroup utility
 */
class ExtendedHTMLFilterXss extends Xss {
  /**
   * Processes a string of HTML attributes.
   *
   * @param string $attributes
   *   The html attribute to process.
   *
   * @return string
   *   Cleaned up version of the HTML attributes.
   */
  protected static function attributes($attributes) {
    $attributes_array = [];
    $mode = 0;
    $attribute_name = '';
    $skip = FALSE;
    $skip_protocol_filtering = FALSE;

    while ($attributes !== '') {
      // Was the last operation successful?
      $working = 0;

      switch ($mode) {
        case 0:
          // Attribute name, href for instance.
          if (preg_match('/^([-a-zA-Z][-a-zA-Z0-9]*)/', $attributes, $match)) {
            $attribute_name = strtolower($match[1]);
            $skip = (
              strpos($attribute_name, 'on') === 0 ||
              strpos($attribute_name, '-') === 0 ||
              // Ignore long attributes to avoid unnecessary processing
              // overhead.
              strlen($attribute_name) > 96
            );

            // Values for attributes of type URI should be filtered for
            // potentially malicious protocols (for example, an href-attribute
            // starting with "javascript:"). However, for some non-URI
            // attributes performing this filtering causes valid and safe data
            // to be mangled. We prevent this by skipping protocol filtering on
            // such attributes.
            // @see \Drupal\Component\Utility\UrlHelper::filterBadProtocol()
            // @see http://www.w3.org/TR/html4/index/attributes.html
            $skip_protocol_filtering = strpos($attribute_name, 'data-') === 0 || in_array($attribute_name, [
                'title',
                'alt',
                'rel',
                'property'
              ]);

            $working = $mode = 1;
            $attributes = preg_replace('/^[-a-zA-Z][-a-zA-Z0-9]*/', '', $attributes);
          }
          break;

        case 1:
          // Equals sign or valueless ("selected").
          if (preg_match('/^\s*=\s*/', $attributes)) {
            $working = 1;
            $mode = 2;
            $attributes = preg_replace('/^\s*=\s*/', '', $attributes);
            break;
          }

          if (preg_match('/^\s+/', $attributes)) {
            $working = 1;
            $mode = 0;
            if (!$skip) {
              $attributes_array[] = $attribute_name;
            }
            $attributes = preg_replace('/^\s+/', '', $attributes);
          }
          break;

        case 2:
          // Attribute value, a URL after href= for instance.
          if (preg_match('/^"([^"]*)"(\s+|$)/', $attributes, $match)) {
            $value = $skip_protocol_filtering ? $match[1] : self::filterAttribute($attribute_name, $match[1]);

            if (!$skip) {
              $attributes_array[] = "$attribute_name=\"$value\"";
            }
            $working = 1;
            $mode = 0;
            $attributes = preg_replace('/^"[^"]*"(\s+|$)/', '', $attributes);
            break;
          }

          if (preg_match("/^'([^']*)'(\s+|$)/", $attributes, $match)) {
            $value = $skip_protocol_filtering ? $match[1] : self::filterAttribute($attribute_name, $match[1]);

            if (!$skip) {
              $attributes_array[] = "$attribute_name='$value'";
            }
            $working = 1;
            $mode = 0;
            $attributes = preg_replace("/^'[^']*'(\s+|$)/", '', $attributes);
            break;
          }

          if (preg_match("%^([^\s\"']+)(\s+|$)%", $attributes, $match)) {
            $value = $skip_protocol_filtering ? $match[1] : self::filterAttribute($attribute_name, $match[1]);

            if (!$skip) {
              $attributes_array[] = "$attribute_name=\"$value\"";
            }
            $working = 1;
            $mode = 0;
            $attributes = preg_replace("%^[^\s\"']+(\s+|$)%", '', $attributes);
          }
          break;
      }

      if ($working === 0) {
        // Not well formed; remove and try again.
        $attributes = preg_replace('/
          ^
          (
          "[^"]*("|$)     # - a string that starts with a double quote, up until the next double quote or the end of the string
          |               # or
          \'[^\']*(\'|$)| # - a string that starts with a quote, up until the next quote or the end of the string
          |               # or
          \S              # - a non-whitespace character
          )*              # any number of the above three
          \s*             # any number of whitespaces
          /x', '', $attributes);
        $mode = 0;
      }
    }

    // The attribute list ends with a valueless attribute like "selected".
    if ($mode === 1 && !$skip) {
      $attributes_array[] = $attribute_name;
    }

    return $attributes_array;
  }

  /**
   * Determine which filtering method to use on an attribute.
   *
   * @param string $attribute_name
   *   The name of the attribute to be used in the check.
   * @param string $attribute_value
   *   The value of the attribute to be filtered.
   *
   * @return string
   *   The filtered string.
   */
  protected static function filterAttribute(string $attribute_name, string $attribute_value) {
    if ($attribute_name === 'style') {
      return static::filterStyleProperties($attribute_value);
    }
    return UrlHelper::filterBadProtocol($attribute_value);
  }

  /**
   * Filter the properties of an inline "style" attribute.
   *
   * @param string $attribute_value
   *   The properties to be checked and filtered.
   *
   * @return string
   *   The filtered string.
   */
  protected static function filterStyleProperties(string $attribute_value) {
    $allowed_style_props = \Drupal::configFactory()->get('extended_html_filter.settings')->get('allowed_style_props');
    $allowed_style_props = preg_split('/\r\n?|\n/', $allowed_style_props);

    $filtered_props = [];
    foreach (explode(';', $attribute_value) as $prop) {
      // Remove whitespaces
      $prop = trim($prop);
      // Check if the property is in the whitelist.
      if (preg_match('/^([a-z\-]+?):.+?$/', $prop, $matches) && in_array($matches[1], $allowed_style_props, true)) {
        $filtered_props[] = $prop;
      }
    }

    if (!empty($filtered_props)) {
      $filtered_props = implode(';', $filtered_props);
      // Add semicolon at the end if it does not exist.
      return substr($filtered_props, -1) === ';' ? $filtered_props : $filtered_props . ';';
    }

    return '';
  }

}

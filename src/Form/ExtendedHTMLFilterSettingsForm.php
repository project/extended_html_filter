<?php

namespace Drupal\extended_html_filter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class ExtendedHTMLFilterSettingsForm.
 */
class ExtendedHTMLFilterSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'extended_html_filter.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'extended_html_filter_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('extended_html_filter.settings');

    $form['allowed_style_props'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed Style Properties'),
      '#description' => $this->t('Whitelist inline style properties (e.g. width, height), separate properties with newlines, leave it empty to prevent using inline style properties.'),
      '#default_value' => $config->get('allowed_style_props'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('extended_html_filter.settings')
      ->set('allowed_style_props', $form_state->getValue('allowed_style_props'))
      ->save();
  }

  /**
   * Returns translatable form name
   *
   * @return TranslatableMarkup
   *   Translatable title.
   */
  public function getTitle(){
    return t('Extended HTML Filter Settings');
  }

}
